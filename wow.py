import cv2
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--scale', type=float, default=0.5, help='Skala Image')
opt = parser.parse_args()

center_coordinates = [0,0]
cX = 0
cY = 0
db = open("./data.txt", "a")
radius = 20
state = 0
_Scale = opt.scale

def main():
    global center_coordinates, cX, cY, db, radius, state, _Scale

    _Path = "./map.jpg"

    img = cv2.imread(_Path, cv2.IMREAD_COLOR)
    img_width = img.shape[1]
    img_height = img.shape[0]

    center_coordinates = [cX, cY]
    radius = 20
    radius2 = 5
    color = (0,0,255)
    thickness = 2
    thickness2 = -1
    font = cv2.FONT_HERSHEY_SIMPLEX

    db = open("./data.txt", "r")
    len_db = len(db.read().split())
    db.close()

    while True:
        center_coordinates = [cX, cY]

        img = cv2.imread(_Path, -1)
        img_width = img.shape[1]
        img_height = img.shape[0]
        img = cv2.putText(img, f'Image Size : ({int(img_width*_Scale)},{int(img_height*_Scale)})', (10,50), font, 2, color, 3, cv2.LINE_AA)

        dim = (int(img_width*_Scale), int(img_height*_Scale))
        img = cv2.resize(img, dim, interpolation = cv2.INTER_CUBIC)

        if state == 0:
            img = cv2.circle(img, center_coordinates, radius, color, thickness)
            img = cv2.circle(img, center_coordinates, radius2, color, thickness2)
            start = center_coordinates
            end = [center_coordinates[0], center_coordinates[1]-30]
            img = cv2.line(img, start, end, color, thickness)
            img = cv2.putText(img, f'({cX},{cY})', end, font, 0.5, color, 1, cv2.LINE_AA)

        db = open("./data.txt", "r")
        db_data = db.read().split()
        if len(db_data) == 0:
            len_db = len(db_data)
        for i,x in enumerate(db_data):
            coor = x.split(",")
            if i<len_db:
                scalling = _Scale/float(coor[4])
                coorX = int(coor[0])*scalling
                coorX = int(coorX)
                coorY = int(coor[1])*scalling
                coorY = int(coorY)
            else:
                coorX = int(coor[0])
                coorY = int(coor[1])

            img = cv2.circle(img, (coorX, coorY), radius, (255,0,0), thickness)
            img = cv2.circle(img, (coorX, coorY), radius2, (255,0,0), thickness2)
            if len(coor)==5:
                if i<len_db:
                    scalling = _Scale/float(coor[4])
                    endX = int(coor[2])*scalling
                    endY = int(coor[3])*scalling
                else:
                    endX = int(coor[2])
                    endY = int(coor[3])
            else:
                endX = cX
                endY = cY
            
            endX = int(endX)
            endY = int(endY)

            start = (coorX, coorY)
            end = [endX, endY]
            img = cv2.line(img, start, end, (255,0,0), thickness)
            img = cv2.putText(img, f'({coorX},{coorY})', end, font, 0.5, (255,0,0), 1, cv2.LINE_AA)
        db.close()

        cv2.imshow("Map", img)
        cv2.setMouseCallback('Map', click_event)

        if cv2.waitKey(1)==ord("q"):
            cv2.destroyAllWindows()
            break

def click_event(event, x, y, flags, params):
    global cX, cY, db, radius, state, _Scale

    cX = x
    cY = y
    if event == cv2.EVENT_LBUTTONDOWN:
        if state == 0:
            db = open("./data.txt", "a")
            db.write(f"{cX},{cY},")
            db.close()
            state = 1
        elif state == 1:
            db = open("./data.txt", "a")
            db.write(f"{cX},{cY},{_Scale}\n")
            db.close()
            state = 0
        
    if event == cv2.EVENT_RBUTTONDOWN:
        db = open("./data.txt", "r")
        data = ""
        for i,x in enumerate(db.read().split()):
            coor = x.split(",")
            skala = float(coor[4])
            scalling = _Scale/skala
            coorX = int(int(coor[0])*scalling)
            coorY = int(int(coor[1])*scalling)
            endX = int(int(coor[2])*scalling)
            endY = int(int(coor[3])*scalling)
            
            r_kuadrat = (int(coorX) - cX)**2 + (int(coorY) - cY)**2
            
            if r_kuadrat<=radius**2:
                pass
            else:
                skala = float(coor[4])
                coorX = int(coor[0])
                coorY = int(coor[1])
                endX = int(coor[2])
                endY = int(coor[3])
                data += f"{coorX},{coorY},{endX},{endY},{skala}\n"
        db.close
        db = open("./data.txt", "w")
        db.write(data)
        db.close()
    

if __name__ == "__main__":
    main()
